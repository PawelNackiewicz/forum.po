package pl.opole.po.forumpo.model;

import lombok.*;
import org.ocpsoft.prettytime.PrettyTime;
import pl.opole.po.forumpo.service.BeanUtil;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Topic extends Auditable {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    @NotEmpty(message = "Wprowadz tytuł")
    private String title;

    @NonNull
    @NotEmpty(message = "Wprowadź opis")
    private String description;

    @OneToMany(mappedBy = "topic")
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "topic")
    private List<Vote> votes = new ArrayList<>();

    private int voteCount = 0;


    public void addComment(Comment comment) {
        comments.add(comment);
    }


    public String getPrettyTime() {
        PrettyTime pt = BeanUtil.getBean(PrettyTime.class);
        pt.setLocale(new Locale("pl","PL"));
        return pt.format(convertToDateViaInstant(getCreationDate()));
    }

    private Date convertToDateViaInstant(LocalDateTime dateToConvert) {
        return java.util.Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
    }
}