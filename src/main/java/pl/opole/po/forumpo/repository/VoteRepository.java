package pl.opole.po.forumpo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.opole.po.forumpo.model.Vote;

public interface VoteRepository extends JpaRepository<Vote,Long> {
}
