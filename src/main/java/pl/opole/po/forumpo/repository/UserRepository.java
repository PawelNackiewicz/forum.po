package pl.opole.po.forumpo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.opole.po.forumpo.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByEmail(String email);

    Optional<User> findByEmailAndActivationCode(String email,String activationCode);
}
