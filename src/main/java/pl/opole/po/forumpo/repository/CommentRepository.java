package pl.opole.po.forumpo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.opole.po.forumpo.model.Comment;

public interface CommentRepository extends JpaRepository <Comment,Long> {
}
