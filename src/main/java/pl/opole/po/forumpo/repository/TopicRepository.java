package pl.opole.po.forumpo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.opole.po.forumpo.model.Topic;

public interface TopicRepository extends JpaRepository <Topic,Long> {
}