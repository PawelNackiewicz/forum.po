package pl.opole.po.forumpo.service;

import org.springframework.stereotype.Service;
import pl.opole.po.forumpo.model.Role;
import pl.opole.po.forumpo.repository.RoleRepository;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findByName(String name){
        return roleRepository.findByName(name);
    }
}
