package pl.opole.po.forumpo.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.opole.po.forumpo.model.Topic;
import pl.opole.po.forumpo.model.Vote;
import pl.opole.po.forumpo.repository.TopicRepository;
import pl.opole.po.forumpo.repository.VoteRepository;

import java.util.Optional;

@RestController
public class VoteController {

    private VoteRepository voteRepository;
    private TopicRepository topicRepository;

    public VoteController(VoteRepository voteRepository, TopicRepository topicRepository) {
        this.voteRepository = voteRepository;
        this.topicRepository = topicRepository;
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/vote/topic/{topicId}/direction/{direction}/votecount/{voteCount}")
    public int vote(@PathVariable Long topicId, @PathVariable short direction, @PathVariable int voteCount){
        Optional<Topic> optionalTopic = topicRepository.findById(topicId);
        if(optionalTopic.isPresent()){
            Topic topic = optionalTopic.get();
            Vote vote = new Vote(direction,topic);
            voteRepository.save(vote);
            int updatedVoteCount = voteCount+direction;
            topic.setVoteCount(updatedVoteCount);
            topicRepository.save(topic);
            return updatedVoteCount;
        }
        return voteCount;
    }
}