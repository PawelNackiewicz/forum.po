package pl.opole.po.forumpo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.opole.po.forumpo.model.User;
import pl.opole.po.forumpo.service.UserService;

import javax.validation.Valid;


@Controller
@RequestMapping("user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(TopicController.class);

    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @Secured({"ROLE_ADMIN"})
    @GetMapping("/list")
    public String user(Model model){
        model.addAttribute("users",userService.findAll());
        return "user/list";
    }

    @DeleteMapping("/drop/{id}")
    public String dropUserById(@PathVariable Long id, RedirectAttributes redirectAttributes){
        try
        {
            logger.info("Starting delete user by id");
            userService.deleteById(id);
            logger.info("User with id "+id+" droped!");
            redirectAttributes.addFlashAttribute("success",true);
            return "redirect:/user/list";
        }catch (Exception e){
            logger.warn(e.toString());
            redirectAttributes.addFlashAttribute("success",false);
            return "/user/list";
        }
    }
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("user", user);
        return "user/viewProfile";
    }
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.warn("Fail during edit user");
        }else {
            user = userService.findById(id).orElseThrow(()-> new IllegalArgumentException("Invalid user Id:"+id));

        }
        userService.save(user);
        model.addAttribute("users", userService.findAll());
        return "index";
    }

}
