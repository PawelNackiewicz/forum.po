package pl.opole.po.forumpo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.opole.po.forumpo.model.Comment;
import pl.opole.po.forumpo.model.Topic;
import pl.opole.po.forumpo.repository.CommentRepository;
import pl.opole.po.forumpo.service.TopicService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class TopicController {
    private static final Logger logger = LoggerFactory.getLogger(TopicController.class);

    private final TopicService topicService;
    private final CommentRepository commentRepository;

    public TopicController(TopicService topicService, CommentRepository commentRepository) {
        this.topicService = topicService;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/")
    public String topic(Model model){
        model.addAttribute("topics",topicService.findAll());
        return "topic/list";
    }
    @GetMapping("/topic/{id}")
    public String read(@PathVariable Long id, Model model) {
        Optional<Topic> topic = topicService.findById(id);
        if( topic.isPresent() ) {
            Topic currentTopic = topic.get();
            Comment comment = new Comment();
            comment.setTopic(currentTopic);
            model.addAttribute("comment",comment);
            model.addAttribute("topic",currentTopic);
            model.addAttribute("success", model.containsAttribute("success"));
            return "topic/view";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/topic/submit")
    public String newTopicForm(Model model) {
        model.addAttribute("topic",new Topic());
        return "topic/submit";
    }

    @PostMapping("/topic/submit")
    public String createTopic(@Valid Topic topic, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if( bindingResult.hasErrors() ) {
            logger.info("Validation errors were found while submitting a new topic.");
            model.addAttribute("topic",topic);
            return "topic/submit";
        } else {
            // save our link
            topicService.save(topic);
            logger.info("New topic was saved successfully");
            redirectAttributes
                    .addAttribute("id",topic.getId())
                    .addFlashAttribute("success",true);
            return "redirect:/topic/{id}";
        }
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/topic/comments")
    public String addComment(@Valid Comment comment, BindingResult bindingResult){
        if( bindingResult.hasErrors() ){
            logger.info("There was a problem adding a new comment.");
        } else {
            commentRepository.save(comment);
            logger.info("New comment was saved successfully.");
        }
        return "redirect:/topic/" + comment.getTopic().getId();
    }
    @DeleteMapping("/topic/drop/{id}")
    public String dropTopicById(@PathVariable Long id, RedirectAttributes redirectAttributes){
        try
        {
            logger.info("Starting delete topic by id");
            topicService.deleteById(id);
            logger.info("Topic with id "+id+" droped!");
            redirectAttributes.addFlashAttribute("successDeleteTopic",true);
            return "redirect:/";
        }catch (Exception e){
            logger.warn(e.toString());
            redirectAttributes.addFlashAttribute("successDeleteTopic",false);
            return "/";
        }
    }
    @GetMapping("/topic/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Topic topic = topicService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid topic Id:" + id));
        model.addAttribute("topic", topic);
        return "topic/editTopic";
    }


}
