package pl.opole.po.forumpo.initializer;

import org.apache.tomcat.util.buf.UEncoder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.opole.po.forumpo.model.Comment;
import pl.opole.po.forumpo.model.Role;
import pl.opole.po.forumpo.model.Topic;
import pl.opole.po.forumpo.model.User;
import pl.opole.po.forumpo.repository.CommentRepository;
import pl.opole.po.forumpo.repository.RoleRepository;
import pl.opole.po.forumpo.repository.TopicRepository;
import pl.opole.po.forumpo.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private TopicRepository topicRepository;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private CommentRepository commentRepository;

    private Map<String, User> users = new HashMap<>();

    public DatabaseLoader(TopicRepository topicRepository, CommentRepository commentRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.topicRepository = topicRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void run(String... args) throws Exception {
//        // add users and roles
//        addUsersAndRoles();
//
//        Map<String,String> topics = new HashMap<>();
//        topics.put("Kto zagra na Piastonaliach?","Jak myslicie kto w tym roku zagra na piastcah?");
//        topics.put("Kiedy zaczyna sie sesja?","Hej kiedy w tym roku bedzie sesja na WB?");
//        topics.put("Integracja w cinie","Kto dzis wybiera sie na integracje, zbieramy ludzi do turnieju we flanki");
//        topics.put("Turniej flanek","Zapisy na turniej flanek 2019 ponizej");
//        topics.put("COS, Godziny otwarcia","Dnia 1.05- 15.05. Cos będzie otwarty w godzinach 9 - 14");
//        topics.put("Odrabianie zajec.","Informuje ze programowanie wspolbiezne z Panem dr. Koziolem  dla grupy L4 zostanie odrobione dnia....");
//        topics.put("Zajęcia odwolane","Dr Mgr. Pawła Majewskiegi nie bedzie dnia 33.13.9101r");
//        topics.put("Nauka programowania.","Szukam osoby która pomozr z programowaniem w C++");
//        topics.put("Stypendia","Wie ktos jaki jest próg na informatyce na rektora?");
//
//        topics.forEach((k,v) -> {
//            User u1 = users.get("user@gmail.com");
//            User u2 = users.get("super@gmail.com");
//            Topic topic = new Topic(k,v);
//            if(k.startsWith("K")) {
//                topic.setUser(u1);
//            } else {
//                topic.setUser(u2);
//            }
//
//            topicRepository.save(topic);
//
//            Comment spring = new Comment("komentarz 1",topic);
//            Comment security = new Comment("komentarz 2",topic);
//            Comment pwa = new Comment("komentarz 3",topic);
//            Comment comments[] = {spring,security,pwa};
//            for(Comment comment : comments) {
//                commentRepository.save(comment);
//                topic.addComment(comment);
//            }
//        });
//
//        long topicCount = topicRepository.count();
//        System.out.println("Number of topics in the database: " + topicCount );

   }

    private void addUsersAndRoles() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String secret = "{bcrypt}" + encoder.encode("password");

        Role userRole = new Role("ROLE_USER");
        roleRepository.save(userRole);
        Role adminRole = new Role("ROLE_ADMIN");
        roleRepository.save(adminRole);

        User admin = new User("admin@gmail.com",secret,true,"Joe","Admin","all");
        admin.addRole(adminRole);
        admin.setConfirmPassword(secret);
        userRepository.save(admin);
        users.put("admin@gmail.com",admin);

        User user = new User("user@gmail.com",secret,true,"Joe","User","all");
        user.addRole(userRole);
        user.setConfirmPassword(secret);
        userRepository.save(user);
        users.put("user@gmail.com",user);


        User master = new User("super@gmail.com",secret,true,"Super","User","all");
        master.addRoles(new HashSet<>(Arrays.asList(userRole,adminRole)));
        master.setConfirmPassword(secret);
        userRepository.save(master);
        users.put("super@gmail.com",master);

        User grzegorz = new User("g.golisz@studen.po.edu.pl",secret,true,"Grzegorz", "Golisz","WEAiI");
        grzegorz.addRole(userRole);
        grzegorz.setConfirmPassword(secret);
        userRepository.save(grzegorz);
        users.put("g.golisz@studen.po.edu.pl",grzegorz);

        User pawelM = new User("p.marcinkow@student.po.edu.pl",secret,true,"Paweł","Marcinków","WEAiI");
        pawelM.addRole(userRole);
        pawelM.setConfirmPassword(secret);
        userRepository.save(pawelM);
        users.put("p.marcinkow@student.po.opole.edu.pl",pawelM);
    }
}
